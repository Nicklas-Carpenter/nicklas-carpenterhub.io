---
layout: post
title:  "Hello, world!"
date:   2023-03-28
categories: jekyll update
tags: meta personal
author: Nicklas Carpenter
---

Hello, world! These are the words virtually every programmer writes at least
once as they make their first forray into a new programming language. Given my
background and the type of content that will most likely end up on this blog,
they seem appropriate as an introduction here. If you are reading this, you
have stumbled onto my blog, Derangements. It's a bit bare-bones at this time of
writing, but hopefully I'll be able to add more soon; I have somewhat of a
backlog of things I want to write about. But before adding any of that, I like
to take the time (space?) in this initial post to establish a few things.

### Who am I?
I am Nicklas Carpenter, a computer engineer by trade and educational
background. You can read a little more about my background on my [GitHub
profile page](https://github.com/Nicklas-Carpenter). I absolutely love what I
get to do; engineering is my hobby as well as my job. I always enjoy learning
more. I've found that learning for learning's sake is not only fun, but has
helped me in my job more times than I can count.

### Why start a blog? 
Starting a blog is something I've wanted to do for a while now. The biggest
barrier has been figuring out how to host it (subject for a later post). A lot
could be said on the issue, but there are two primary reasons that come to mind
when I think of why I wanted to start a blog. The first is that, is that I love
figuring out how things work on a fundamental level; there is a soft of beauty
in conceptual understanding. I think beauty is something that not only draws
ourselves in, but compels us to share it with others. This blog is my way of
doing that.

The second major reason I wanted to start a blog is related to the first.
Starting out, I found that a lot of things I was interested in were very
inaccessibly to a beginner, more so than necessary. I have *a lot* more to say
about this, but that will require a blog post of its own. The core of it is
this: I found it hard to find answers to the questions I was interested in and
I hope this blog can be a resource to people with similar interests in a
similar position as I was (and often still am).

### Why the name "Derangments"?
In mathematics, a derangment is a way to arrange a set of things such nothing
is in the right order. Imagine you order takeout sandwiches with a group and
when you take a bit of your sandwich you realize you were given the wrong one:
you have someone else's sandwich and someone else has yours. Now imagine that
everyone in the group has the same experience. All the right sandwiches are
there, if you put all the sandwiches in a pile everyone should be able to find
the sandwich they orderd, but every person was given someone else's sandwich.
In this scenario, that particular matching between people and sandwiches is a
derangement.

Generally, a derangement is not a desired outcome. In fact, if perfect ordering
is the desired outcome, then a derangement is the maximally non-optimal
outcome. When I encountered the term in a math class, I knew immediately I
wanted to use it as the name for something. Derangment is a term that comes to
mind when I think of what I like to do. For various reasons, understanding
something at a fundamental level often means looking at it in a non-optimal
setting. I can't tell you how much I've learned through breaking things, or
figuring out how to fix something that was broken. With software and
electronics especially, knowing how something works means understanding its
limitations, edge cases, and fundamental assumptions (that can be broken). It's
within the derangements where true learning often happens.

### And without further ado...
I think that wraps up everything I had for now. If you want to get in contact,
feel free to email me at <carpenter.nicklas@gmail.com>. Other than that, stay
tuned for more posts: I have a lot I want to write, but writing takes time and
creative energy that I don't always have. 
